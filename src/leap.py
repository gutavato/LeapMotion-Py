import sys
import _header

# Import Leap
try:
    import Leap
except:
    print "Error: Leap Importation"
    sys.exit()

class LeapListener(Leap.Listener):

    def on_connect(self, controller):
        print "Connected"

    def on_frame(self, controller):
        frame = controller.frame()

        print "Frame id: %d, hands: %d, fingers: %d" % (
            frame.id, len(frame.hands), len(frame.fingers))

def main():
    # Create a listener and controller
    listener = LeapListener()
    controller = Leap.Controller()

    # Refer the Listener to the Controller
    controller.add_listener(listener)

    # Keep this process running until Enter is pressed
    print ("Press Enter to quit...")
    try:
        sys.stdin.readline()
    except KeyboardInterrupt:
        pass
    finally:
        # Remove the listener when done
        controller.remove_listener(listener)

if __name__ == "__main__":
    main()
