import sys
import _header

# Import OpenGL
try:
    from OpenGL.GL import *
    from OpenGL.GLU import *
    from OpenGL.GLUT import *
except:
    print "Error: pyOpenGl Importation"
    sys.exit()

# Import Leap
try:
    import Leap
except:
    print "Error: Leap Importation"
    sys.exit()

# Import Hand
import hand

# The main function
def main():

    # Initialize OpenGL
    glutInit(sys.argv)

    # Set display mode
    glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE)

    # Set size and position of window size
    #glutInitWindowSize(1200, 1000)
    #glutInitWindowPosition(100, 100)

    # Create window with given title
    #title = "Leap OpenGl Integration"
    #glutCreateWindow(title)

    # Create a controller for leap
    #controller = Leap.Controller()

    # The callback for display function
    #glutDisplayFunc(display)

    # The callback for reshape function
    #glutReshapeFunc(reshape)

    # The callback that represent the animated model
    #glutIdleFunc(scene.animate)

    # Run the GLUT main loop until the user closes the window.
    #glutMainLoop()    

# Call the main function
if __name__ == "__main__":
    main()