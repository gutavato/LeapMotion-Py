PPE-Leap Project Repository
===========================

This project include an SDK provided by the [official Leap Motion website](https://developer.leapmotion.com/ "Leap Motion website"). 

### Details of the SDK used

**Version:** 2.3.1  
**Build:** 31549  

### Dependencies

Make sure you have [Python](https://www.python.org/downloads/ "Python 2.7.x") in version 2.7.x

On Ubuntu:
```bash
sudo apt-get install python-opengl
```
On Windows:  
- Download and install from [this link](https://pypi.python.org/pypi/PyOpenGL/3.0.2 "PyOpenGl Windows")

### Links
- [Python 2.7.x](https://www.python.org/downloads/ "Python 2.7.x").
- [Leap SDK v2.3.1 (LTS)](https://developer.leapmotion.com/sdk/v2/ "Leap SDK v2.3.1 (LTS)").
- [PyOpenGl Windows](https://pypi.python.org/pypi/PyOpenGL/3.0.2 "PyOpenGl Windows").

### Author
Gustavo Zanoni - 
[LinkedIn](https://br.linkedin.com/in/gustavo-zanoni-6371a791 "LinkedIn Link")